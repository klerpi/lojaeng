/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import model.Conexao;
import view.TelaCadastroProduto;
import view.TelaPrincipal;

/**
 *
 * @author Iuri
 */
public class ControleProduto implements ActionListener{
    
    private TelaCadastroProduto telaCadastroProduto;
    private Connection conexao;
    private RNProduto rnProduto;

    

    ControleProduto(TelaPrincipal telaPrincipal, Connection conexao) {
        this.telaCadastroProduto = new TelaCadastroProduto(telaPrincipal,true);
        
        this.telaCadastroProduto.getjButtonCadastrar().addActionListener(this);
        this.telaCadastroProduto.getjButtonBuscar().addActionListener(this);
        this.telaCadastroProduto.getjButtonDeletar().addActionListener(this);
        this.telaCadastroProduto.getjButtonEditar().addActionListener(this);
        
        this.conexao = conexao;
        this.rnProduto = new RNProduto(this.telaCadastroProduto,this.conexao);
        this.telaCadastroProduto.setVisible(true);
    }
    

    @Override
    public void actionPerformed(ActionEvent evento) {
        if(evento.getSource() == this.telaCadastroProduto.getjButtonCadastrar()){    
            cadastrar();
        }
        if(evento.getSource() == this.telaCadastroProduto.getjButtonBuscar()){    
            buscar();
        }
        if(evento.getSource() == this.telaCadastroProduto.getjButtonDeletar()){    
            deletar();
        }
        if(evento.getSource() == this.telaCadastroProduto.getjButtonEditar()){    
            editar();
        }
    }

    
    private void cadastrar() {
        this.rnProduto.cadastrar();
    }

    private void buscar() {
        this.rnProduto.buscar();
    }

    private void deletar() {
        this.rnProduto.deletar();
    }

    private void editar() {
        this.rnProduto.editar();
    }
}
