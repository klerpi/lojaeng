/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Conexao;
import model.Produto;
import model.ProdutoDao;
import view.TelaCadastroProduto;
/**
 *
 * @author Iuri
 */
public class RNProduto {
    
    private TelaCadastroProduto telaCadastroProduto;
    
    private Connection conexao;
    private ProdutoDao produtoDao;
    private int id = 0;
    

    public RNProduto(TelaCadastroProduto telaCadastroProduto, Connection conexao) {
        this.conexao = conexao;
        this.produtoDao = new ProdutoDao(this.conexao);
        this.telaCadastroProduto = telaCadastroProduto;
        this.listar();
    }
    private boolean validacampos(){
        if(this.telaCadastroProduto.getjTextFieldNome().getText().equals("")){
            return false;
        }
        else if(this.telaCadastroProduto.getjTextAreaDescricao().getText().equals("")){
            return false;
        }
        else if(this.telaCadastroProduto.getjTextFieldPreco().getText().equals("")){
            return false;
        }
        else if(this.telaCadastroProduto.getjTextFieldQuantidade().getText().equals("")){
            return false;
        }
        else{
            return true;
        }
    }
    public void cadastrar() {
         if (validacampos()){
            Produto p = new Produto();
     
            p.setNomeProd(this.telaCadastroProduto.getjTextFieldNome().getText());
            p.setDestricao(this.telaCadastroProduto.getjTextAreaDescricao().getText());
            p.setPreco(this.telaCadastroProduto.getjTextFieldPreco().getText());
            p.setQuantidadeProd(this.telaCadastroProduto.getjTextFieldQuantidade().getText());
          
            
            if(produtoDao.buscar(p)){
                this.produtoDao.alterar(p);
                JOptionPane.showMessageDialog(null, "Cadastrado editado com sucesso ");
            }else{
                this.produtoDao.inserir(p);
            }
            this.limpar();
            listar();
            
        }else{
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!","Erro",JOptionPane.ERROR_MESSAGE);
        }
    }

    void buscar() {
        Produto p = new Produto();
        ArrayList<Produto> produto = new ArrayList<>();
        String nome = this.telaCadastroProduto.getjTextFieldBusca().getText();
     
        produto = produtoDao.PesquisaNome(nome);
      
        if(p.getId() < 0){
            
            JOptionPane.showMessageDialog(null, "funcionario não existe");
        }else{
            listaDados(produto);
         
        }
    }

    void deletar() {
        int item = this.telaCadastroProduto.getjTableResultados().getSelectedRow();
        if(item>=0){
            if(JOptionPane.showConfirmDialog(this.telaCadastroProduto, "Deseja realmente excluir?", "Confirmação de exclusão", 
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0){
                Produto p = new Produto();
                p.setId((int)this.telaCadastroProduto.getModelo().getValueAt(item, 0));
                if(produtoDao.excluir(p)){
                    this.telaCadastroProduto.getModelo().removeRow(item);                
                }
                limpar();
                listar();
            } 
        } else{
            JOptionPane.showMessageDialog(this.telaCadastroProduto, "Selecione um item");
        }
    }

    void editar() {
        int item = this.telaCadastroProduto.getjTableResultados().getSelectedRow();
        
        if(item >= 0){
            this.id = (int)this.telaCadastroProduto.getModelo().getValueAt(item,0 );
            
            Produto p = produtoDao.pesquisar(this.id);
            
            System.out.println("produtoooooo");
            System.out.println(p);
            this.telaCadastroProduto.getjTextFieldNome().setText(p.getNomeProd());
            this.telaCadastroProduto.getjTextFieldQuantidade().setText(p.getQuantidadeProd());
            this.telaCadastroProduto.getjTextFieldPreco().setText(p.getPreco());
            this.telaCadastroProduto.getjTextAreaDescricao().setText(p.getDestricao());
            
            
            listar();
        }else{
            JOptionPane.showMessageDialog(this.telaCadastroProduto, "Selecione um item");
        }
    }

    private void listar() {
        listaDados(produtoDao.listar());
        
    }

    private void limpar() {
        this.telaCadastroProduto.limpar();
    }

    private void listaDados(ArrayList<Produto> listaProduto) {
        
       impaTabela();
        for(int i=0;i<listaProduto.size();i++){
            adicionaTabela(listaProduto.get(i).getId(),
                           listaProduto.get(i).getNomeProd(),
                           listaProduto.get(i).getQuantidadeProd(),
                           listaProduto.get(i).getPreco(),
                           listaProduto.get(i).getDestricao());
                          
        }   
    }
    private void adicionaTabela(Object... objects){
        this.telaCadastroProduto.getModelo().addRow(objects);
    }
    

    private void impaTabela() {
        int linhas = this.telaCadastroProduto.getModelo().getRowCount();
        for(int i=0;i<linhas;i++){
            this.telaCadastroProduto.getModelo().removeRow(0);
        }
    }
    
    

}
