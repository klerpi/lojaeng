package controller;

import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Conexao;
import model.VendaDao;
import view.TelaVenda;
import model.Produto;
import com.lowagie.text.Document;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Iuri
 */
public class RNVenda {
    
    private TelaVenda telaVenda;
    
    private Connection conexao;
    private VendaDao vendaDao;
    
    private Produto p = new Produto();
    
    ArrayList<Produto> carrinho = new ArrayList<>();
    
    private int id = 0;

    public RNVenda(TelaVenda telaVenda, Connection conexao) {
        this.conexao = conexao;
        this.vendaDao = new VendaDao(this.conexao);
        this.telaVenda = telaVenda;
        this.listar();
    }

    

    void pesquisar() {
        p = new Produto();
        ArrayList<Produto> produtos = new ArrayList<>();
        String nome = this.telaVenda.getjTextFieldBusca().getText();
   
        produtos  = vendaDao.PesquisaProduto(nome);
      
        if(p.getId()< 0){
            JOptionPane.showMessageDialog(null, "Produto não existe");
            System.out.println("apertou buscar produto");
        }else{
            listaDados(produtos);

        }
    }
     private void mostraTabela(Produto p){
       ArrayList<Produto> listaDeProdutos = new ArrayList<>();
       listaDeProdutos.add(p); 
       listaDados(listaDeProdutos);
    }
    private void listaDados(ArrayList<Produto> listaDeProdutos){
        limpaTabela();
        for(int i=0;i<listaDeProdutos.size();i++){
            adicionaTabela(listaDeProdutos.get(i).getId(),
                           listaDeProdutos.get(i).getNomeProd(),
                           listaDeProdutos.get(i).getPreco(),
                           listaDeProdutos.get(i).getQuantidadeProd(),
                           listaDeProdutos.get(i).getDestricao());
        }   
        
    }
    private void listaDados2(ArrayList<Produto> listaDeProdutos){
        limpaTabela2();
        for(int i=0;i<listaDeProdutos.size();i++){
            adicionaTabela2(listaDeProdutos.get(i).getId(),
                           listaDeProdutos.get(i).getNomeProd(),
                           listaDeProdutos.get(i).getPreco(),
                           listaDeProdutos.get(i).getQuantidadeProd(),
                           listaDeProdutos.get(i).getDestricao());
        }   
        
    }
    private void adicionaTabela(Object... objects){
        this.telaVenda.getModelo().addRow(objects);
    }
    private void adicionaTabela2(Object... objects){
        this.telaVenda.getModelo2().addRow(objects);
    }
      
    public void limpaTabela(){
        int linhas = this.telaVenda.getModelo().getRowCount();
        for(int i=0;i<linhas;i++){
            this.telaVenda.getModelo().removeRow(0);
        }
    }
    public void limpaTabela2(){
        int linhas = this.telaVenda.getModelo2().getRowCount();
        for(int i=0;i<linhas;i++){
            this.telaVenda.getModelo2().removeRow(0);
        }
    }

    void comprar() throws DocumentException {
         Document documento = new Document(PageSize.A4, 30,20,20,30);
        
        try {
            PdfWriter.getInstance(documento, new FileOutputStream("documento.pdf"));
            documento.open();
            Paragraph paragrafo = new Paragraph("NOTA NÃO FISCAL");
            documento.add(paragrafo);
            
            PdfPTable tabela = new PdfPTable(4); 
            PdfPCell cabecalho = new PdfPCell(new Paragraph("tabela"));
            
            cabecalho.setHorizontalAlignment(Element.ALIGN_CENTER);
            cabecalho.setBorder(PdfPCell.NO_BORDER);
            
            cabecalho.setColspan(4);
            
            tabela.addCell(cabecalho);
            tabela.addCell("NOME");
            tabela.addCell("PREÇO");
            tabela.addCell("QUANTIDADE");
            tabela.addCell("DESCRIÇÃO");
    
            int total = 0;
            int totalqtd = 0;
            int preco;
            int qtd ;
            for(int i=0;i<carrinho.size();i++){
                
                preco = Integer.parseInt(carrinho.get(i).getPreco());
                qtd = Integer.parseInt(carrinho.get(i).getQuantidadeProd());
                totalqtd += qtd;
                total += preco;
                tabela.addCell(carrinho.get(i).getNomeProd());
                tabela.addCell(carrinho.get(i).getPreco());
                tabela.addCell(carrinho.get(i).getQuantidadeProd());
                tabela.addCell(carrinho.get(i).getDestricao());
            }   
            
            tabela.addCell("TOTAL");
            tabela.addCell(String.valueOf(total));
            tabela.addCell(String.valueOf(totalqtd));
            tabela.addCell("");
            documento.add(tabela);
            
        } catch (FileNotFoundException ex) {
            System.out.println("Erro no pfd" + carrinho.size());
        }finally{
            documento.close();
        }
        try {
            Desktop.getDesktop().open(new File("documento.pdf"));
        } catch (IOException ex) {
            Logger.getLogger(pdf.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void addCarrinho() {
        int item = this.telaVenda.getjTableResultados().getSelectedRow();
        
        
       
        if(item >= 0){
            this.id = (int)this.telaVenda.getModelo().getValueAt(item,0 );
            
            p = this.vendaDao.pesquisar(this.id);
            
            carrinho.add(p);
            listaDados2(carrinho);
            
                    
            listar();
        }else{
            JOptionPane.showMessageDialog(this.telaVenda, "Selecione um item");
        }
        
    }

    private void listar() {
        listaDados(vendaDao.listar());
    }


    private static class pdf {

        public pdf() {
        }
    }

    

    
    
    

}
