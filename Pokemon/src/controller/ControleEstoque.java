package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import model.Conexao;
import view.TelaAdocao;
import view.TelaPrincipal;
/**
 *
 * @author Iuri
 */
public class ControleEstoque implements ActionListener{
    
    TelaAdocao telaEstoque;
    Connection conexao;
    RNAdocao rnAdocao;

    ControleEstoque(TelaPrincipal telaPrincipal) {
        this.conexao = Conexao.getConexao();
        this.telaEstoque = new TelaAdocao(telaPrincipal,true);
        
        this.rnAdocao = new RNAdocao(telaEstoque);
        
        this.telaEstoque.getjButtonAdotar().addActionListener(this);
        this.telaEstoque.getjButtonBuscar().addActionListener(this);
        
        
        this.telaEstoque.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(this.telaEstoque.getjButtonAdotar()== e.getSource()){
            adotar();
        }
        if(this.telaEstoque.getjButtonBuscar()== e.getSource()){
            buscar();
        }
    }

    private void adotar(){
        rnAdocao.adotar();
    }
    
    private void buscar(){
        rnAdocao.buscar();
    }
}
