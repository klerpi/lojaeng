/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Iuri
 */
public class ProdutoDao {
    
    private final Connection conexao;
    
    public ProdutoDao(Connection conexao) {
        this.conexao = conexao;
    }

    public boolean buscar(Produto produto) {
        String sql = "select * from tb_Produto where Nome like ?";
        Produto p = new Produto();
        
        PreparedStatement pst;
        ResultSet rs;
        
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1, produto.getNomeProd());
            rs = pst.executeQuery();
            while(rs.next()){
                p.setId(rs.getInt("idProduto"));
                p.setNomeProd(rs.getString("Nome"));
                p.setPreco(rs.getString("preco"));
                p.setQuantidadeProd(rs.getString("Estoque"));
                p.setDestricao(rs.getString("Descricao"));
                
                
            }
            rs.close();
            pst.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possivel busca.");
            System.out.println(ex);
        }
        
        return(p.getId()>0);
    }

    

    public void alterar(Produto produto) {
        String sql = "update tb_Produto set Nome = ? , Preco = ?, Estoque = ?, Descricao = ? where Nome = ?";
 
      
        if(buscar(produto)){
            try {
                
                PreparedStatement pst = conexao.prepareStatement(sql);
                
                pst.setString(1,produto.getNomeProd());
                pst.setString(2,produto.getPreco());
                pst.setString(3,produto.getQuantidadeProd());
                pst.setString(4,produto.getDestricao());
                pst.setString(5,produto.getNomeProd());
                
                pst.execute();
                pst.close();                
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Não foi possivel alterar.");
                System.out.println(ex);
            }
        } else{
            JOptionPane.showMessageDialog(null, "Pessoa não cadastrada.");
        }     
        
        
         
    }

    public void inserir(Produto produto) {
        if(!buscar(produto)){
            String sql = "insert into tb_Produto(Nome, Preco, Estoque, Descricao) value (?,?,?,?)";
            
            PreparedStatement pst;
            try {
                pst = conexao.prepareStatement(sql);
                pst.setString(1,produto.getNomeProd());
                pst.setString(2,produto.getPreco());
                pst.setString(3,produto.getQuantidadeProd());
                pst.setString(4,produto.getDestricao());
                
                pst.execute();
                pst.close();
                System.out.println("o sistema tentou cadastrar o produto");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Não foi possivel inserir.");
                System.out.println(ex);
               
            }
            JOptionPane.showMessageDialog(null, "Produto cadastrado com sucesso.");
            
        }else{
            JOptionPane.showMessageDialog(null, "Não foi possivel inserir. Pessoa já cadastrada.");
        }
    }
    public ArrayList<Produto> PesquisaNome(String nome){
        String sql = "select * from tb_Produto where  Nome like ?";
        ArrayList<Produto> produto = new ArrayList<>();
        
        PreparedStatement pst;
        ResultSet rs;
        
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1,nome + "%");
            rs = pst.executeQuery();
            while(rs.next()){
                Produto p = new Produto();
                p.setId(rs.getInt("idProduto"));
                p.setNomeProd(rs.getString("Nome"));
                p.setPreco(rs.getString("preco"));
                p.setQuantidadeProd(rs.getString("Estoque"));
                p.setDestricao(rs.getString("Descricao"));
                produto.add(p);
                
            }
            
            rs.close();
            pst.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possivel listar.");
            System.out.println(ex);
        }
        
        return produto;
        
    }
    public ArrayList<Produto> listar(){
        String sql = "select * from tb_Produto order by Nome";
        
        ArrayList<Produto> lista = new ArrayList<>();
        
        PreparedStatement pst;
        ResultSet rs;
        try{
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                Produto p = new Produto();
                p.setId(rs.getInt("idProduto"));
                p.setNomeProd(rs.getString("Nome"));
                p.setPreco(rs.getString("preco"));
                p.setQuantidadeProd(rs.getString("Estoque"));
                p.setDestricao(rs.getString("Descricao"));
   
                lista.add(p);
            }
            rs.close();
            pst.close();
            
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Não foi possivel listar.");
            System.out.println(ex);
        }
        return lista;
        
    }

    public boolean excluir(Produto produto) {
        String sql = "delete from tb_Produto where idProduto = ?";
        boolean result = false;
                
        if(!buscar(produto)){
            try {
                PreparedStatement pst = conexao.prepareStatement(sql);
                pst.setInt(1, produto.getId());
                result = pst.execute();
                pst.close();                
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Não foi possivel excluir.");
                System.out.println(ex);
            }
            
        } else{
            JOptionPane.showMessageDialog(null, "Produto não cadastrada.");
        }
        
        return result; 
    }

    public Produto pesquisar(int id) {
        String sql = "select * from tb_Produto where idProduto = ?";
        Produto p = new Produto();
        System.out.println("o id é");
        System.out.println(id);
        PreparedStatement pst;
        ResultSet rs;
        
        try {
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, id);
            rs = pst.executeQuery();
            while(rs.next()){
                p.setId(rs.getInt("idProduto"));
                p.setNomeProd(rs.getString("Nome"));
                p.setPreco(rs.getString("preco"));
                p.setQuantidadeProd(rs.getString("Estoque"));
                p.setDestricao(rs.getString("Descricao"));
            }
            rs.close();
            pst.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possivel pesquisar.");
            System.out.println(ex);
        }
        return p;
    }

}
