/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Iuri
 */
public class VendaDao {

    
    
    private final Connection conexao;

    public VendaDao(Connection conexao) {
       this.conexao = conexao;
    }

    

    public ArrayList<Produto> PesquisaProduto(String nome) {
        String sql = "select * from tb_Produto where  Nome like ?";
        
        ArrayList<Produto> produto = new ArrayList<>();
        PreparedStatement pst;
        ResultSet rs;
        
        try {
            pst = conexao.prepareStatement(sql);
            pst.setString(1,nome + "%");
            rs = pst.executeQuery();
            while(rs.next()){
                Produto p = new Produto();
                p.setId(rs.getInt("idProduto"));
                p.setNomeProd(rs.getString("Nome"));
                p.setPreco(rs.getString("Preco"));
                p.setQuantidadeProd(rs.getString("Estoque"));
                p.setDestricao(rs.getString("Descricao"));
                produto.add(p);
                
            }
            
            rs.close();
            pst.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possivel listar.");
            System.out.println(ex);
        }
        
        return produto;
    }
    public ArrayList<Produto> listar() {
         String sql = "select * from tb_Produto order by Nome,Preco";
        
        ArrayList<Produto> lista = new ArrayList<>();
        
        PreparedStatement pst;
        ResultSet rs;
        try{
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                Produto p = new Produto();
                p.setId(rs.getInt("idProduto"));
                p.setNomeProd(rs.getString("Nome"));
                p.setPreco(rs.getString("Preco"));
                p.setQuantidadeProd(rs.getString("Estoque"));
                p.setDestricao(rs.getString("Descricao"));
                lista.add(p);
            }
            rs.close();
            pst.close();
            
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Não foi possivel listar.");
            System.out.println(ex);
        }
        
        return lista;
    }
    
    //pesquisa o produto selecionado pelo seu ID
    public Produto pesquisar(int id){
        String sql = "select * from tb_Produto where idProduto = ?";
        Produto p = new Produto();
       
        PreparedStatement pst;
        ResultSet rs;
        
        try {
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, id);
            rs = pst.executeQuery();
            while(rs.next()){
                p.setId(rs.getInt("idProduto"));
                p.setNomeProd(rs.getString("Nome"));
                p.setPreco(rs.getString("Preco"));
                p.setQuantidadeProd(rs.getString("Estoque"));
                p.setDestricao(rs.getString("Descricao"));
            }
            rs.close();
            pst.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possivel pesquisar.");
            System.out.println(ex);
        }
        return p;
    }
   

}
