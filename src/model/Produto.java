package model;

/**
 *
 * @author Iuri
 */
public class Produto {
    
    private int id;    
    private String nomeProd;
    private String preco;
    private String quantidadeProd;
    private String destricao;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getNomeProd() {
        return nomeProd;
    }

    public void setNomeProd(String nomeProd) {
        this.nomeProd = nomeProd;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getQuantidadeProd() {
        return quantidadeProd;
    }

    public void setQuantidadeProd(String quantidadeProd) {
        this.quantidadeProd = quantidadeProd;
    }

    public String getDestricao() {
        return destricao;
    }

    public void setDestricao(String destricao) {
        this.destricao = destricao;
    }
    

}
