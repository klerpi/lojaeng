/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.util.ArrayList;
import model.Conexao;
import model.Pokemon;
import model.PokemonDao;
import view.TelaAdocao;

/**
 *
 * @author Leonardo
 */
public class RNAdocao {
    TelaAdocao tela;
    Connection conexao;
    PokemonDao pokemonDao;
    
    public RNAdocao(TelaAdocao tela){
        this.tela = tela;
        this.conexao = Conexao.getConexao();
        this.pokemonDao = new PokemonDao();
        this.listar();
    }
    
    public void listar(){
        listarDados(pokemonDao.listar());
    }
    
    private void listarDados(ArrayList<Pokemon> lista){
        limparTabela();
        
        for (int i=0;i<lista.size();i++){
            adicionaNaTabela(lista.get(i).getNome(), lista.get(i).getEstoque(),
                    lista.get(i).getTipo1(), lista.get(i).getTipo2(),
                    lista.get(i).getHabilidade(), lista.get(i).getDescricao(),
                    lista.get(i).getPreco());
        }
    }
    
    private void adicionaNaTabela(Object ... object){
        this.tela.getModelo().addRow(object);
    }
    
    private void limparTabela(){
        int linhas = this.tela.getModelo().getRowCount();
        
        for(int i=0;i<linhas;i++){
            this.tela.getModelo().removeRow(0);
        }
    }
    
    public void buscar(){
        listarDados(this.pokemonDao.buscarCool(this.tela.getjTextFieldBusca().getText()));
    }
    
    public void adotar(){
        Pokemon pokemon = new Pokemon();
        int linha = this.tela.getjTableResultados().getSelectedRow();
        pokemon.setNome(this.tela.getModelo().getValueAt(linha, 0).toString());
        pokemon.setEstoque(Integer.parseInt(this.tela.getModelo().getValueAt(linha, 1).toString()));
        pokemonDao.adotar(pokemon);
        
        listar();
    }
}
