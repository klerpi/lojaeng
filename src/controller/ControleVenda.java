package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;
import model.Conexao;
import model.Produto;
import view.TelaVenda;
import view.TelaPrincipal;

/**
 *
 * @author Iuri
 */
public class ControleVenda implements ActionListener{
    
    private TelaVenda telaVenda;
    
    private RNVenda rnVenvda;
    private Connection conexao;

    public ControleVenda(TelaPrincipal telaPrincipal,Connection conexao) {
        
        this.telaVenda = new TelaVenda(telaPrincipal,true);
        this.telaVenda.getjButtonBuscar().addActionListener(this);
        this.telaVenda.getjButtonAdd().addActionListener(this);
        this.telaVenda.getjButtonCompar().addActionListener(this);
        
        this.conexao = conexao;
        this.rnVenvda = new RNVenda(this.telaVenda,this.conexao);
        this.telaVenda.setVisible(true);
        
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        if(evento.getSource() == this.telaVenda.getjButtonBuscar()){
            pesquisar();
        }
        if(evento.getSource() == this.telaVenda.getjButtonAdd()){
            addCarrinho();
        }
        if(evento.getSource() == this.telaVenda.getjButtonCompar()){
            comprar();
        }
        
    }

    private void pesquisar() {
        this.rnVenvda.pesquisar();
    }

    private void comprar() {
      
        this.rnVenvda.comprar();
    }

    private void addCarrinho() {
        this.rnVenvda.addCarrinho();
    }
    

}
