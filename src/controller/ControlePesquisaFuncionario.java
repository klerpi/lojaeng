/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import model.Conexao;
import view.TelaPesquisaFuncionario;
import view.TelaPrincipal;
import view.TelaCadastroFuncionario;
/**
 *
 * @author Iuri
 */
public class ControlePesquisaFuncionario implements ActionListener{
    
    private TelaPesquisaFuncionario telaPesquisaFuncionario;
    private Connection conexao;
    private RNPesquisaFuncionario rnPesquisaFuncionario ;
    private ControleFuncionario controleFuncionario;
    
    ControlePesquisaFuncionario(TelaPrincipal telaPrincipal, Connection conexao) {
        
        this.telaPesquisaFuncionario = new TelaPesquisaFuncionario(telaPrincipal,true);
        this.telaPesquisaFuncionario.getjButtonCadastrarFuncionario().addActionListener(this);
        this.telaPesquisaFuncionario.getjButtonPesquisar().addActionListener(this);
        
        this.conexao = conexao;
        this.rnPesquisaFuncionario = new RNPesquisaFuncionario(this.telaPesquisaFuncionario,this.conexao);
        
        this.telaPesquisaFuncionario.setVisible(true);
        System.out.println("clicou no pesquise funcionario");
    }
    @Override
    public void actionPerformed(ActionEvent evento) {
        
        
        if(evento.getSource() == this.telaPesquisaFuncionario.getjButtonPesquisar()){    
            pesquisar();
        }
        
        /*
        if(evento.getSource() == this.telaPesquisaFuncionario.getjButtonCadastrarFuncionario() ||
                evento.getSource() == telaPesquisaFuncionario.getjButtonCadastrarFuncionario()){
            this.controleFuncionario = new ControleFuncionario(telaPesquisaFuncionario,conexao);
        }*/
    }    

    private void pesquisar() {
       this.rnPesquisaFuncionario.pesquisar();
    }
  

    
}
