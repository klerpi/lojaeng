package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import model.Conexao;
import view.TelaAdocao;
import view.TelaPrincipal;
/**
 *
 * @author Iuri
 */
public class ControleEstoque implements ActionListener{
    
    private TelaAdocao telaEstoque;
    private Connection conexao;
    private RNAdocao rNAdocao;

    ControleEstoque(TelaPrincipal telaPrincipal) {
        this.conexao = Conexao.getConexao();
        this.telaEstoque = new TelaAdocao(telaPrincipal,true);
        
        this.rNAdocao = new RNAdocao(this.telaEstoque);
        
        this.telaEstoque.getjButtonAdotar().addActionListener(this);
        this.telaEstoque.getjButtonBuscar().addActionListener(this);
        this.telaEstoque.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(this.telaEstoque.getjButtonAdotar() == ae.getSource()){
            adotar();
        }
        if(this.telaEstoque.getjButtonBuscar()== ae.getSource()){
            buscar();
        }
    }
    
    public void adotar(){
        this.rNAdocao.adotar();
    }
    public void buscar(){
        this.rNAdocao.buscar();
    }

}
