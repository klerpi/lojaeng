/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Funcionario;
import model.FuncionarioDao;
import view.TelaCadastroFuncionario;
import view.TelaPesquisaFuncionario;

/**
 *
 * @author Iuri
 */
public class RNPesquisaFuncionario {
    
    private TelaCadastroFuncionario telaCadastroFuncionario;
    private TelaPesquisaFuncionario telaPesquisaFuncionario;
    private Connection conexao;
    private FuncionarioDao funcDao;
    
    public RNPesquisaFuncionario(TelaPesquisaFuncionario telaPesquisaFuncionario, Connection conexao) {
        
        this.conexao = conexao;
        this.funcDao = new FuncionarioDao(this.conexao);
        this.telaPesquisaFuncionario = telaPesquisaFuncionario;
        this.listar();
        
    }

    void pesquisar() {
        Funcionario f = new Funcionario();
        String nome = this.telaPesquisaFuncionario.getjTextPesquisar().getText();
        ArrayList<Funcionario> funcionario = new ArrayList<>();
        funcionario = funcDao.PesquisaNome(nome);
       
       /* System.out.println(f);*/
        /*funcDao.PesquisaNome(nome);*/
        if(f.getId() < 0){
            JOptionPane.showMessageDialog(null, "funcionario não existe");
        }else{
           listaDados(funcionario);
        }
    }
    private boolean validacampos(){
        if(this.telaPesquisaFuncionario.getjTextPesquisar().getText().equals("")){
            return false;
        }
        else{
            return true;
        }
    }
    private void mostraTabela(Funcionario f) {
       
       ArrayList<Funcionario> listaDeFuncionario = new ArrayList<>();
       listaDeFuncionario.add(f); 
       listaDados(listaDeFuncionario);
    }   
    private void listaDados(ArrayList<Funcionario> listaFuncionario){
        limpaTabela();
        for(int i=0;i<listaFuncionario.size();i++){
            adicionaTabela(listaFuncionario.get(i).getId(),
                           listaFuncionario.get(i).getNomeFunc(),
                           listaFuncionario.get(i).getCargo(),
                           listaFuncionario.get(i).getCpf(),
                           listaFuncionario.get(i).getCelular(),
                           listaFuncionario.get(i).getEmail());
        }   
        
    }
    public void limpaTabela(){
        int linhas = this.telaPesquisaFuncionario.getModelo().getRowCount();
        for(int i=0;i<linhas;i++){
            this.telaPesquisaFuncionario.getModelo().removeRow(0);
        }
    }
    private void adicionaTabela(Object... objects){
        this.telaPesquisaFuncionario.getModelo().addRow(objects);
    }
    private void listar() {
        listaDados(funcDao.listar());
    }
}
